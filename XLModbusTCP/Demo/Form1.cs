﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XLModbus.Plcs.Modbus;

namespace Demo
{
    public partial class Form1 : Form
    {
        private IModbus _modbus;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _modbus = new ModbusTcp(txtIP.Text, Convert.ToInt32(txtPORT.Text), Convert.ToByte(txtADDR.Text));
            try
            {
                _modbus.Open();
                button1.Enabled = false;
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"异常：{ex}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                _modbus.Close();
                button1.Enabled = true;
                button2.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"异常：{ex}");
            }
        }
        private System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        private void button3_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            var bl = _modbus.GetCoil(Convert.ToInt32(txtColiAddr.Text));
            stopwatch.Stop();
            MessageBox.Show($"读到的状态为：{bl} 耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            var bl = _modbus.GetCoil(Convert.ToInt32(txtColiAddr.Text), Convert.ToInt32(txtCoilNumber.Text));
            stopwatch.Stop();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in bl)
            {
                stringBuilder.AppendLine($"{item}");
            }
            MessageBox.Show($"读到的状态为：\r\n{stringBuilder}耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            _modbus.SetCoil(Convert.ToInt32(txtColiAddr.Text), checkBox1.Checked);
            stopwatch.Stop();
            MessageBox.Show($"写入耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            _modbus.SetCoil(Convert.ToInt32(txtColiAddr.Text), 
                new bool[] { 
                checkBox1.Checked, 
                checkBox1.Checked , 
                checkBox1.Checked , 
                checkBox1.Checked , 
                checkBox1.Checked , 
                checkBox1.Checked , 
                checkBox1.Checked });
            stopwatch.Stop();
            MessageBox.Show($"写入耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            var bl = _modbus.ReadBitRegister(Convert.ToInt32(txtRegisterAddr.Text), Convert.ToInt32(txtRegisterOff.Text));
            stopwatch.Stop();
            MessageBox.Show($"读到的状态为：\r\n{bl}耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            _modbus.SetRegister(Convert.ToInt32(txtRegisterAddr.Text),Convert.ToInt16(txtRegisterValue.Text));
            stopwatch.Stop();
            MessageBox.Show($"耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            _modbus.SetBitRegister(Convert.ToInt32(txtRegisterAddr.Text), Convert.ToInt32(txtRegisterOff.Text), checkBox2.Checked);
            //_modbus.SetBitRegister(地址,旧值,偏移,布尔量);一般建议使用这个方法写位，因为会覆盖其他值，所以看需求
            stopwatch.Stop();
            MessageBox.Show($"写入耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            txtRegisterValue.Text = _modbus.ReadRegister<double>(Convert.ToInt32(txtRegisterAddr.Text)).ToString();
            stopwatch.Stop();
            MessageBox.Show($"耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            var list = _modbus.ReadRegister(Convert.ToInt32(txtRegisterAddr.Text), Convert.ToInt32(txtRegisterNumber.Text));
            stopwatch.Stop();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in list)
            {
                stringBuilder.AppendLine(item.ToString());
            }
            MessageBox.Show($"读到的值：\r\n{stringBuilder}耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            List<object> list = new List<object>();
            for (int i = 0; i < 5; i++)
            {
                list.Add((float)(i + 0.23));
            }
            _modbus.SetRegister<float>(Convert.ToInt32(txtRegisterAddr.Text), list);
            stopwatch.Stop();
            MessageBox.Show($"耗时：{stopwatch.ElapsedMilliseconds} ms");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
            var model = _modbus.ReadClass<TestModel>(Convert.ToInt32(txtRegisterAddr.Text));
            stopwatch.Stop();
            MessageBox.Show($"耗时：{stopwatch.ElapsedMilliseconds} ms");
        }
    }
}
